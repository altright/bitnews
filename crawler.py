from lxml import html 
from lxml import etree
from lxml.cssselect import CSSSelector 
import pymongo 
from bson.objectid import ObjectId
import requests
from newsplease import NewsPlease
from bs4 import BeautifulSoup

client = pymongo.MongoClient("mongodb://localhost:27017/")

db = client['bitnews']

col = db['crawl_articles']

def hindutimes(): 
	#key value mapping for global categories 
	category_mapping = {
		"lok-sabha-elections" : "politics", 
		"world-news" : "politics", 
		"tech"		 : "technology", 
		"sports-news" : "sports", 
		"cricket"     : "sports"
	}

	base_hindu_uri = "https://www.hindustantimes.com/"
	base_hindu_sec_uri = base_hindu_uri 

	hindu_times_categories =[ x for x in category_mapping.keys() ] 



	for category in hindu_times_categories: 
		req_url = base_hindu_sec_uri + category + "/"

		#load the section page 
		page = requests.get(req_url)

		tree = html.fromstring(page.content)

		title_elems = tree.cssselect('#scroll-container > ul > li > div > div.media-body')

		for link in title_elems: 
			href = link.cssselect('div > a')[0].get('href')
			print(href)
			if(col.find({'url': href}).count() > 0):
				print('article already exists')
			else: 
				article = NewsPlease.from_url(href)

				x = col.insert_one({
				"url" : href,
				"news_category" : category_mapping[category], 
				"source"		: "hindustantimes", 
				"date_publish" 	: article.date_publish,
				"description"	: article.description,
				"image_url"		: article.image_url ,
				"article_text"	: article.text,
				"article_title" : article.title,
				"article_url" 	: article.url 
				})
				print("Headline: " + str(article.title))
				print("Article Inserted " + " hindustantimes" ,href)
		


# NY TIMES CRAWLER 
ny_times_categories = ['politics', 'business', 'technology', 'science', 'health', 'sports']

BASE_SITE_URI = "https://www.nytimes.com"

BASE_SECTION_URI = BASE_SITE_URI + "/section/"

url_arr = []

def nytimes():

	for category in ny_times_categories: 
		req_url = BASE_SECTION_URI + category

		# load the section page 
		page = requests.get(req_url)

		# url extraction 
		tree = html.fromstring(page.content)

		title_elems = title_elem = tree.xpath('//*[@id="stream-panel"]/div[1]/ol/li')

		for link in title_elems:
			href = link.cssselect('li > div > div > a')[0].get('href')
			
			if(col.find({'url': href}).count() > 0):
				print('article already exists')
			else:

				article = NewsPlease.from_url(BASE_SITE_URI + href)

				x = col.insert_one({
				"url" : href,
				"news_category" : category, 
				"source"		: "nytimes", 
				"date_publish" 	: article.date_publish,
				"description"	: article.description,
				"image_url"		: article.image_url ,
				"article_text"	: article.text,
				"article_title" : article.title,
				"article_url" 	: article.url 
				})

				print("Article Inserted "  + " nytimes" ,href)

if __name__=="__main__":
	######## INSERT CRAWLER FUNCTION HERE ##################	

