import os 
import pymongo 
from bson.objectid import ObjectId

client = pymongo.MongoClient("mongodb://localhost:27017/")

db = client['bitnews']

col = db['train_articles']

script_dir = os.path.dirname(__file__)


rel_path_article = "sample_data/sumdata/train/train.article.txt"
rel_path_headline = "sample_data/sumdata/train/train.title.txt" 

abs_article_path = os.path.join(script_dir,rel_path_article)
abs_headline_path = os.path.join(script_dir, rel_path_headline)

with open(abs_article_path) as f: 
	content = f.readlines()

content = [x.strip() for x in content]



with open(abs_headline_path) as f: 
	headlines = f.readlines()

headlines = [x.strip() for x in headlines]

for i in range(len(content)): 

	try:
		col.insert_one({
			"text": content[i], 
			"title": headlines[i]
		})

	except: 
		print('error inserting')