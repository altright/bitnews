from summarizer import Summarizer
from nltk.tokenize import sent_tokenize, word_tokenize
from pprint import pprint
from nltk.sentiment.vader import SentimentIntensityAnalyzer as SIA
from featuredAlgo import extract_sentences, extract_key_phrases
import re
import spacy
from collections import Counter
import en_core_web_sm

nlp = en_core_web_sm.load()

# Summary word count of different algo 
def summary_wc_algo(content, title, sent_length = 5):

	all_summary_wc = {
		"textrank"     : len(word_tokenize(Summarizer.textrank_summarizer(content))),
		"lexrank"      : len(word_tokenize(Summarizer.lexrank_summarizer(content))),
		"lsa"	       : len(word_tokenize(Summarizer.lsa_summarizer(content))),
		"featuredAlgo" : len(word_tokenize(Summarizer.featured_summarizer(content,title))),
	}

	return all_summary_wc

# Sentiments of headline and content 
def senti_points_text_head(content, title): 
	sia = SIA()

	sentiment_arr = {
		"headline": "",
		"content" : ""
	}

	sentiment_scores_headline = sia.polarity_scores(title)
	sentiment_scores_content = sia.polarity_scores(content)

	sentiment_arr['headline'] = sentiment_scores_headline 
	sentiment_arr['content']  = sentiment_scores_content

	return sentiment_arr

# Sentence wise record change in sentiments 
def senti_points_sentence_wise(content): 
	sia = SIA()

	sentences = sent_tokenize(content) 

	sentence_senti = {}

	for i in range(len(sentences)):
		sentence_senti[i] = sia.polarity_scores(sentences[i])

	return sentence_senti

def _helperFunc_key_phrase(key_phrases, sentences):
	sentence_key_phrase_freq = {}
	
	index = 0

	for sent in sentences: 
		count = 0
		for phrase in key_phrases: 
			count = count + len([m.start() for m in re.finditer(phrase.lower(),sent.lower())])
		sentence_key_phrase_freq[index] = count 

		index = index + 1 

	return sentence_key_phrase_freq


# find the occurence of key phrases in each sentence
def key_phrase_freq_algo(content, title, sent_length = 5): 
	key_phrases = extract_key_phrases(title)
	
	key_phrase_algo = {
		"textrank"     : _helperFunc_key_phrase(key_phrases, sent_tokenize(Summarizer.textrank_summarizer(content, sent_length))),
		"lexrank"      : _helperFunc_key_phrase(key_phrases, sent_tokenize(Summarizer.lexrank_summarizer(content, sent_length))),
		"lsa" 	       : _helperFunc_key_phrase(key_phrases, sent_tokenize(Summarizer.lsa_summarizer(content, sent_length))),
		"featuredAlgo" : _helperFunc_key_phrase(key_phrases, sent_tokenize(Summarizer.featured_summarizer(content,title, sent_length))), 
	}

	return key_phrase_algo 

def spacy_ner(content):
	doc = nlp(' '.join(word_tokenize(content)))
	arr = [(X.text, X.label_) for X in doc.ents]
	
	ner_tags = {}

	for elems in arr: 
		ner_tags[elems[0]] = elems[1]

	return ner_tags

def spacy_ner_wc(content):
	doc = nlp(' '.join(word_tokenize(content)))
	labels = [x.label_ for x in doc.ents]
	return dict(Counter(labels))

