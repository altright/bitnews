from sumy.parsers.plaintext import PlaintextParser
from sumy.nlp.tokenizers import Tokenizer
from sumy.summarizers.lex_rank import LexRankSummarizer 
from sumy.summarizers.luhn import LuhnSummarizer
from sumy.summarizers.lsa import LsaSummarizer
from sumy.summarizers.text_rank import TextRankSummarizer
from nltk.tokenize import sent_tokenize, word_tokenize
from featuredAlgo import extract_sentences, extract_key_phrases

SENTENCES_COUNT = 5
MAX_WORD_LIMIT = 500 

def word_limiter(text, word_limit = MAX_WORD_LIMIT):
	result_summary = []
	sentences = sent_tokenize(text)
	word_lim = 0
	for sent in sentences: 
		word_lim += len(word_tokenize(sent))
		if(word_lim <= word_limit):
			result_summary.append(sent)
		else:
			result_summary.append(sent)
			break

	return ' '.join(result_summary)


def sentence_limiter(text, sent_count): 
	result_summary = []

	sentences = sent_tokenize(text)

	for i in range(int(sent_count)):
		result_summary.append(sentences[i])

	return ' '.join(result_summary)

class Summarizer: 

	def lexrank_summarizer(text, sent_count = SENTENCES_COUNT):
		parser = PlaintextParser.from_string(text, Tokenizer("english"))
		summarizer = LexRankSummarizer()
		summary = summarizer(parser.document, sent_count) 
		return word_limiter(' '.join([str(sentence) for sentence in summary]))

	def luhn_summarizer(text, sent_count = SENTENCES_COUNT): 
		parser = PlaintextParser.from_string(text, Tokenizer("english"))
		summarizer = LuhnSummarizer()
		summary =summarizer(parser.document,sent_count)
		return word_limiter(' '.join([str(sentence) for sentence in summary])) 



	def lsa_summarizer(text, sent_count = SENTENCES_COUNT): 
		parser = PlaintextParser.from_string(text, Tokenizer("english"))
		summarizer= LsaSummarizer()
		summary =summarizer(parser.document,sent_count)
		return word_limiter(' '.join([str(sentence) for sentence in summary])) 


	def textrank_summarizer(text, sent_count = SENTENCES_COUNT): 
		parser = PlaintextParser.from_string(text, Tokenizer("english"))
		summarizer= TextRankSummarizer()
		summary =summarizer(parser.document,sent_count)
		return word_limiter(' '.join([str(sentence) for sentence in summary])) 


	def featured_summarizer(text, title, sent_count = SENTENCES_COUNT): 
		return sentence_limiter(extract_sentences(text, title), sent_count)


