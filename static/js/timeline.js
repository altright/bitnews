var timeline = {} || timeline; 

timeline.articles = ""; 

timeline.fnLoadArticles = function(){

  // load articles from backend on page load 
    $.ajax({
      url : "http://localhost:5000/abstract_articles", 
      contentType: 'application/json', 
      type: 'GET', 
      dataType: "json",

      success: function(response){
        timeline.articles = response.data; 
        timeline.fnshowArticles(); 

      }, 
      error: function(error){
        swal("error",{
            icon: "error",
        }); 
      }
    }); 
}

timeline.fnShowTitle =function(key){
  let val = timeline.articles; 


  $(`[name='btn-${key}']`).hide(); 
  $(`[name='spin-${key}']`).show(); 
  $('#bar-chart').remove(); 
  $('#chart-evaluation').append('<canvas id="bar-chart" width="800" height="450"></canvas>'); 

  setTimeout(function(){ 
  new Chart(document.getElementById("bar-chart"), {
    type: 'bar',
    data: {
      labels: ["BLEU", "ROUGE_1", "ROUGE_2", "ROUGE_l", "ROUGE_BE"],
      datasets: [
        {
          label: "Evaluation Scores",
          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
          data: [(Math.round(parseFloat(val[key].BLEU) * 100) / 100),
          (Math.round(parseFloat(val[key].ROUGE_1) * 100) / 10),
          (Math.round(parseFloat(val[key].ROUGE_2) * 100) / 10),
          (Math.round(parseFloat(val[key].ROUGE_l) * 100) / 10),
          (Math.round(parseFloat(val[key].ROUGE_be) * 100) / 10)]
        }
      ]
    },
    options: {
      legend: { display: false },
      title: {
        display: true,
        text: 'Abstractive Summary Evaluation'
      }
    }
});


  $(`[name='spin-${key}']`).hide(); 

  $(`[name="title-${key}"]`).show();
  $(`[name="eval-${key}"]`).show();

},  Math.floor(Math.random() * 1200) + 100  );


}

timeline.fnshowArticles = function(){

	var html = ""; 

	timeline.articles.forEach((val,key) =>{
		let t = val.title; 
		let c = val.text;  
		html+='<li>'; 
		html+= `
    <div class="w3-center">
        <hr/>
        <button onclick="timeline.fnShowTitle(${key})" style='font-size: 1.2em' class='w3-button w3-round-large w3-blue' name="btn-${key}">Generate Headline</button>
        <span id="spin_d" name="spin-${key}" style="display: none"><i class="fa fa-spinner fa-spin" style="font-size:24px"></i></span>

        <b class="w3-red w3-padding" style="display: none; " name="title-${key}">${t}</b>  
        <hr/>

        <fieldset>
          <legend style="color: #fff">Actual Title:</legend>
          <p >${val.ref_title}</p>
        </fieldset>

        <fieldset>
          <legend style="color: #fff">Content:</legend>
          <p >${c}</p>
        </fieldset>

        <fieldset name='eval-${key}' style='display: none'>
          <legend style="color: #fff">Evaluation</legend>
         <button class='w3-button w3-orange' onclick="document.getElementById('id01').style.display='block'">View Evaluation</button>
        </fieldset>

    </div>
    `; 
		html+='</li>'; 
	}); 

	$('#ul-abstract').html(html); 
	$('#arti_count').html(`Articles Count: ${timeline.articles.length}`); 

}


$(document).ready(function(){

  timeline.fnLoadArticles(); 

	  // define variables
  var items = document.querySelectorAll(".timeline li");

  // check if an element is in viewport
  // http://stackoverflow.com/questions/123999/how-to-tell-if-a-dom-element-is-visible-in-the-current-viewport
  function isElementInViewport(el) {
    var rect = el.getBoundingClientRect();
    return (
      rect.top >= 0 &&
      rect.left >= 0 &&
      rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
      rect.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
  }

  function callbackFunc() {
    for (var i = 0; i < items.length; i++) {
      if (isElementInViewport(items[i])) {
        items[i].classList.add("in-view");
      }
    }
  }

  // listen for events
  window.addEventListener("load", callbackFunc);
  window.addEventListener("resize", callbackFunc);
  window.addEventListener("scroll", callbackFunc);
})