// Script to open and close sidebar
function w3_open() {
    document.getElementById("mySidebar").style.display = "block";
    document.getElementById("myOverlay").style.display = "block";
}
 
function w3_close() {
    document.getElementById("mySidebar").style.display = "none";
    document.getElementById("myOverlay").style.display = "none";
}


var header = document.getElementById("myHeader");



// SIGNUP MODAL
var modal = document.getElementById("signup");

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}

// ------------------------------------ MAIN PAGE FUNCTIONS ----------------------------

var page = {} || page; 

page.fnSignUp = function(e){
  
  // Form fields 
  let email = $("[name='email']").val(); 
  let name = $("[name='username']").val(); 
  let dob = $("[name='dob']").val(); 
  let gender = $("[name='gender']:checked").val() ; 
  
  let news_cat = []; 
   $("[name='categories']:checked").each(function(index){
    news_cat.push(this.value );
  }) ;  

  let password = $("[name='password']").val(); 
  
  let form_correct = 1; 

  if(email == ''){
    swal(`Email can't be blank`, {
      icon: "error", 
    });
    form_correct = 0;  
  }
  else if(name == ''){
    swal(`Name can't be blank`, {
      icon: "error", 
    }); 
    form_correct = 0; 
  }

  else if(dob == ''){
    swal(`dob can't be blank`, {
      icon: "error", 
    }); 
    form_correct = 0; 
  }

  else if(gender == ''){
    swal('select a gender',{
      icon: "error", 
    }); 
    form_correct = 0; 
  }

  else if(news_cat.length == 0){
    swal(`select your news preference`,{
      icon: "error",
    }); 
    form_correct = 0; 
  }

  else if(password.length < 6){
    swal('enter a valid 6 character password', {
      icon: "error", 
    }); 
    form_correct = 0; 
  }


  if(form_correct == 1){
      let formData = JSON.stringify({
      "user_email"    : email,  
      "user_name"     : name, 
      "user_dob"      : dob, 
      "user_gender"   : gender, 
      "user_news_cat" : news_cat, 
      "user_password" : password
    }); 

    $.ajax({
      url : "http://localhost:5000/register", 
      data: formData, 
      contentType: 'application/json', 
      type: 'POST', 
      dataType: "json",

      success: function(response){
        if(response.data == 'registered'){
          document.getElementById('signup').style.display='none'
          swal("Thank you for signing up!", {
            icon: "success", 
          })

          $("[name='login_username']").val() = ''; 
          $("[name='login_password']").val() = '';       
        }

        swal(response.data);
      }, 
      error: function(error){
        swal('error'); 
      }
    }); 
  }
}

page.fnLogin = function(){
  let login_user = $("[name='login_username']").val(); 
  let login_pass = $("[name='login_password']").val(); 
  
  let formData = JSON.stringify({
    "username" : login_user, 
    "password" : login_pass
  }); 

  $.ajax({
    url : "http://localhost:5000/login", 
    data: formData, 
    contentType: 'application/json', 
    type: 'POST', 
    dataType: "json",

    success: function(response){

      if(response.data == 'login_success')
        window.location.replace("http://localhost:5000/index");
      else
        swal(response.data); 

    }, 
    error: function(error){
      swal('error'); 
    }
  }); 
}

$(document).ready(function(){
  $('#login_form').submit(function(e){
    e.preventDefault(); 
    page.fnLogin(); 
  });
}); 