var summarizer = summarizer || {};
summarizer.url = window.location.origin + '/summarizer'

$(document).ready(function(){
	console.log("test"); 

	$('form').submit(function(e){
		e.preventDefault();
		var inp_content = $('#inp_content').val(); 
		var sum_type	= $('[name=optradio]:checked').val(); 
		var formData = JSON.stringify({
			"content" 	: inp_content, 
			"algorithm" : sum_type 
		}); 


		$.ajax({
			url : "http://localhost:5000/summarizer", 
			data: formData, 
			contentType: 'application/json', 
			type: 'POST', 
			dataType: "json",

			success: function(response){
				$('#out_content').text(response.data)
			}, 
			error: function(error){
				alert('error'); 
			}
		}); 
	}); 
}); 