addarticle = {} || addarticle; 

// "url" : href,
// 				"news_category" : category_mapping[category], 
// 				"source"		: "hindustantimes", 
// 				"date_publish" 	: article.date_publish,
// 				"description"	: article.description,
// 				"image_url"		: article.image_url ,
// 				"article_text"	: article.text,
// 				"article_title" : article.title,
// 				"article_url" 	: article.url 

addarticle.fnAjaxAdd = function(){
	// Add article to the backend 
	let url = $("[name='url']").val();
	let news_category = $("[name='newscategory']:checked").val(); 
	let news_source = $("[name='news_source']").val(); 
	let date_publish = $("[name='date_publish'").val(); 
	let description = $("[name='description']").val(); 
	let image_url = $("[name='image_url']").val(); 
	let article_text = $("[name='article_text']").val(); 
	let article_url = $("[name='article_url']").val(); 
	let article_title = $("[name='article_title']").val(); 
	
	let formData = JSON.stringify({
		"url" 			: article_url, 
		"news_category" : news_category, 
		"source" 		: news_source, 
		"date_publish" 	: date_publish, 
		"image_url" 	: image_url, 
		"article_text"  : article_text,
		"article_title" : article_title, 
		"article_url"	: article_url, 
		"description"	: description 
	});

	$.ajax({
      url : "http://localhost:5000/add_article", 
      data: formData, 
      contentType: 'application/json', 
      type: 'POST', 
      dataType: "json",

      success: function(response){
      	swal('Your article added to database', {
      		icon: "success"
      	}); 
        
      }, 
      error: function(error){
        alert('error'); 
      }
    }); 
}

$(document).ready(function(){
	var now = new Date();

	var day = ("0" + now.getDate()).slice(-2);
	var month = ("0" + (now.getMonth() + 1)).slice(-2);

	var today = now.getFullYear()+"-"+(month)+"-"+(day) ;

	$("[name='date_publish']").val(today);


	$('#sub_btn').click(function(){
		addarticle.fnAjaxAdd(); 
	}); 
}); 