// Get the modal
var modal = document.getElementById('id01');

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}


function openTab(evt, tabname) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(tabname).style.display = "block";
  evt.currentTarget.className += " active";
}

// page dictionary 
var news_index = news_index || {}; 

news_index.articles = {}; 

news_index.articles_filter = {}; 

news_index.cur_index = 0; 

news_index.sentiment_data = {}; 

news_index.visual_data = {}; 

news_index.cur_sent_length = 7; 

news_index.fnLoadArticles = function(){
  let html = ""; 
  html += `<div class="w3-center" style="width: 50%; margin: 0 auto; ">`; 
    html += `<div class="lds-facebook w3-center"><div></div><div></div><div></div></div>`; 
  html += '</div>'; 
  $('#topnews_section').html(html); 
  $('#fornews_section').html(html); 
  // load articles from backend on page load 
    $.ajax({
      url : "http://localhost:5000/load_articles", 
      contentType: 'application/json', 
      type: 'GET', 
      dataType: "json",

      success: function(response){
        swal("Your bits are ready!",{
            icon: "success",
        }); 

        news_index.articles = response.data; 
        news_index.articles_filter = news_index.articles; 
        news_index.fnDisplayArticles(news_index.articles); 

      }, 
      error: function(error){
        swal("error",{
            icon: "error",
        }); 
      }
    }); 
}

news_index.fnDisplayArticles = function(news_arr){
  // display the articles on the page on page load 
    var html = "";

    let index = 0; 
    news_arr = news_arr.sort((a,b)=>{
        let a_date = parseInt(a.date_publish.split('-')[1]); 
        let b_date = parseInt(b.date_publish.split('-')[1]); 
        
        return b_date - a_date; 

      }); 

    console.log(news_arr);

    for(let row = 1; row <= 10; row++){
      if(news_arr[index])
        html += '<div class="w3-row-padding">';
      
      for(let col = 1; col <= 3; col++){
        if(news_arr[index]){
          html+= '<div class="w3-third w3-container w3-margin-bottom w3-animate-opacity">';
            html+= '<figure class="imghvr-blur box">';
            html+= '<img height="200px;" src="'+ news_arr[index].image_url+ '" alt="Norway" style="width:100%" class=" w3-hover-opacity">';
            html+= '<figcaption class="box">';
            html+= '<p>' + news_arr[index].description  +'</p>';
            html+= '</figcaption>';
            html+= '</figure>';
            html+= '<div class="w3-container w3-white ">';
              html+= '<p><b>'+ news_arr[index].article_title +'</b></p>';
              html+= '<button class="article-button box" onclick="news_index.fnModal('+ index + ')" style="width:auto;">Read</button>';
              html+= '<br>';
                html+= '<p class="w3-tag w3-blue">' + news_arr[index].news_category  +'</p>';
                html+= '<p class="w3-tag w3-orange w3-margin">' + news_arr[index].source  +'</p>';
                html+= '<p class="w3-tag w3-grey">' + news_arr[index].date_publish.split(" ")[0]  +'</p>';
            html+= '</div>';
          html+= '</div>'; 
        }
        index++; 
      }

    if(news_arr[index-1])
      html+= '</div>'; 
    }

  if(news_arr.length == 0){

    $('#topnews_section').html(`<div class="w3-row-padding"><h2>No News Found</h2><div>`); 
  }else{
    $('#topnews_section').html(html); 
  }


  // update for you section 
  html = ""; 


    html += '<div class="w3-row-padding">';
    var len = news_index.articles.length 
    for(var i =1; i <= 3; i++){
        html+= '<div class="w3-third w3-container w3-margin-bottom">';
        html+= '<figure class="imghvr-blur">';
          html+= '<img src="'+ news_index.articles[len-i].image_url+ '" alt="Norway" style="width:100%" class="w3-hover-opacity">';
          html+= '<figcaption>';
          html+= '<p>' + news_index.articles[len-i].description  +'</p>';
          html+= '</figcaption>';
          html+= '</figure>';
          html+= '<div class="w3-container w3-white">';
            html+= '<p ><b>'+ news_index.articles[len-i].article_title +'</b></p>';
            html+= '<button class="article-button" onclick="news_index.fnModal('+ (len-i) + ')" style="width:auto;">Read</button>';
            html+= '<br>';
              html+= '<p class="w3-tag w3-blue">' + news_index.articles[len-i].news_category  +'</p>';
              html+= '<p class="w3-tag w3-orange w3-margin">' + news_index.articles[len-i].source  +'</p>';
              html+= '<p class="w3-tag w3-grey">' + news_index.articles[len-i].date_publish.split(" ")[0]  +'</p>';
          html+= '</div>';
        html+= '</div>'; 
    }
    html+= '</div>';

    $('#fornews_section').html(html); 
}


news_index.fnSummaryAjax = function(art_index, news_arr){
    var inp_content = $('#summary_text').text(); 
    var sum_type  = $('[name=optradio]:checked').val();
    var sentence_length = $('[name="sentence_length_article"]').val(); 
    
    var formData = JSON.stringify({
      "headline"        : news_arr[art_index].article_title, 
      "content"         : news_arr[art_index].article_text, 
      "algorithm"       : sum_type,  
      "sentence_length" : sentence_length
    }); 


    $.ajax({
      url : "http://localhost:5000/summarizer", 
      data: formData, 
      contentType: 'application/json', 
      type: 'POST', 
      dataType: "json",

      success: function(response){
        news_arr[art_index].summary = response.data; 
        $('#summary_text').text(response.data); 
        $('#summary_ner_text').text(response.data); 
        $("#orig_len").text(news_arr[art_index].article_text.split(" ").length + " words"); 
        news_index.ner_highlighter(news_index.visual_data.spacy_ner);
        $(".sum_algo").text(algorithm); 

      }, 
      error: function(error){
        alert('error'); 
      }
    }); 
}


news_index.fnSentimentAjax = function(art_index, news_arr){
      var formData = JSON.stringify({
      "content"   : news_arr[art_index].article_text, 
      "headline"  : news_arr[art_index].article_title 
      }); 

      $.ajax({
      url : "http://localhost:5000/sentiments", 
      data: formData, 
      contentType: 'application/json', 
      type: 'POST', 
      dataType: "json",

      success: function(response){
        news_index.sentiment_data = response.data; 
        $('#headline_sentiment').remove(); 
        $('#content_sentiment').remove(); 
        news_index.fnModalBuildGraph(art_index); 

      }, 
      error: function(error){
        alert('error'); 
      }
    }); 
}



news_index.fnVisualAjax = function(art_index, news_arr, sent_length = news_index.cur_sent_length){
  var formData = JSON.stringify({
    "headline" : news_arr[art_index].article_title, 
    "content"  : news_arr[art_index].article_text, 
    "sentence_length" : sent_length
  });

  $.ajax({
    url: "http://localhost:5000/visualize", 
    data: formData, 
    contentType : 'application/json', 
    type: 'POST', 
    dataType: 'json', 
      success: function(response){
        $('#wc_algo').remove(); 
        $('#headline_sentiment_pi').remove(); 
        $('#content_sentiment_pi').remove(); 
        $('#sentence_wise_senti').remove(); 
        $('#key_phrase_freq').remove();
        $('#spacy_ner').remove();

        news_index.visual_data = response.data; 
        news_index.fnModalBuildGraphAnalytics();
        news_index.ner_highlighter(news_index.visual_data.spacy_ner);

      }, 
      error: function(error){
        alert('error'); 
      }
  }); 
}

news_index.fnModal = function(i){
  $('#id01').css({"display": "block"}); 
  news_index.cur_sent_length = 7; 
  news_index.cur_index = i; 

  $('#summary_text').text(news_index.articles_filter[i].summary); 
  $('#summary_ner_text').text(news_index.articles_filter[i].summary);

  $('#orig_article_link').attr({"href" : news_index.articles_filter[i].article_url}); 
  $('#modal_headline').text(news_index.articles_filter[i].article_title); 
  $(".sum_algo").text('textRank'); 
  $("#orig_len").text(news_index.articles_filter[i].article_text.split(" ").length + " words"); 
  $("#summary_len").text(news_index.articles_filter[i].summary.split(" ").length + " words"); 

  news_index.fnSentimentAjax(i, news_index.articles_filter); 
  news_index.fnVisualAjax(i, news_index.articles_filter);

}; 

news_index.fnModalBuildGraphAnalytics = function(){
  $('#chart_sect_3').append('<canvas id="wc_algo"></canvas>'); 
  $('#chart_sect_4').append('<canvas id="headline_sentiment_pi"></canvas>'); 
  $('#chart_sect_5').append('<canvas id="content_sentiment_pi"></canvas>'); 
  $('#chart_sect_6').append('<canvas id="sentence_wise_senti"></canvas>'); 
  $('#chart_sect_7').append('<canvas id="key_phrase_freq"></canvas>'); 
  $('#chart_sect_8').append('<canvas id="spacy_ner"></canvas>'); 

  // =================== WORD COUNT ======================
  // ===================== CHART =========================
  // =====================================================
  var wc_dict = news_index.visual_data.summary_wc_algo; 
  var x_axis = []; 
  var y_axis = []; 

   $.each(wc_dict, function (key, val) {
        x_axis.push(key); 
        y_axis.push(val); 
  });

  var ctx = document.getElementById('wc_algo').getContext('2d');
  var chart = new Chart(ctx, {
      // The type of chart we want to create
      type: 'bar',
      responsive: true, 
      maintainAspectRatio: false,
      // The data for our dataset
      data: {
          labels: x_axis,
          datasets: [{
              label: "#word counts",
              backgroundColor:[
                  'rgba(0, 51, 204, 0.2)',
                  'rgba(255, 255, 102, 0.2)',
                  'rgba(245, 100, 86, 0.2)',
                  'rgba(255, 51, 0, 0.2)'
              ],
              borderColor:[
                  'rgba(0, 51, 204, 0.2)',
                  'rgba(255, 255, 102, 0.2)',
                  'rgba(245, 100, 86, 0.2)',
                  'rgba(255, 51, 0, 0.2)'
              ] ,
              data: y_axis,
          }]
      },

      // Configuration options go here
      options: {
          title: {
              display: true,
              text: 'Summarized Word Count Comparison'
          }, 
          scales: {
              xAxes: [{
                  stacked: true, 
                  display: true, 
                  scaleLabel: {
                    display: true,
                    labelString: 'Summarization Algorithm (x-axis)'
                  }
              }],
              yAxes: [{
                  stacked: true, 
                  display: true, 
                  scaleLabel: {
                    display: true,
                    labelString: 'Word Count (y-axis)'
                  }
              }]
          }
      }
  });




  // =================== PIE CHART =======================
  // ===================== CHART =========================
  // =================== HEADLINE  =======================
  var head  = news_index.visual_data.senti_points_text_head.headline; 
  var cont   = news_index.visual_data.senti_points_text_head.content; 


  new Chart(document.getElementById("headline_sentiment_pi"), {
      type: 'pie',
      data: {
        labels: ["postiive", "negative", "neutral"],
        datasets: [{
          label: "Sentiment Distribution Title",
          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f"],
          data: [head.pos * 100, head.neg * 100 ,head.neu * 100]
        }]
      },
      options: {
        title: {
          display: true,
          text: 'Sentiments pie chart for headline | Orginal Text'
        }
      }
  });



  new Chart(document.getElementById("content_sentiment_pi"), {
      type: 'pie',
      data: {
        labels: ["postiive", "negative", "neutral"],
        datasets: [{
          label: "Sentiment Distribution Title",
          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f"],
          data: [cont.pos * 100, cont.neg * 100 , cont.neu * 100]
        }]
      },
      options: {
        title: {
          display: true,
          text: 'Sentiments pie chart for Content | Orginal Text'
        }
      }
  });

 // =================== LINE CHART =======================
  // ===================== CHART =========================
  // =================== SENTENCE  =======================

  var sent_dict = news_index.visual_data.senti_points_sentence_wise; 
  var x_axis_senti = []; 
  var y_axis_senti = []; 

   $.each(sent_dict, function (key, val) {
        x_axis_senti.push(key); 
        y_axis_senti.push(val); 
  });

   pos_y = []; 
   neg_y = []; 
   neu_y = []; 

   $.each(y_axis_senti, function (key, val) {
        pos_y.push(val.pos * 100);
        neg_y.push(val.neg * 100); 
        neu_y.push(val.neu * 100);  
  });


  new Chart(document.getElementById("sentence_wise_senti"), {
    type: 'line',
    data: {
      display: false,
      labels: x_axis_senti,
      datasets: [{ 
          data: pos_y,
          label: "POS",
          borderColor: "#3e95cd",
          fill: false
        }, { 
          data: neg_y,
          label: "NEG",
          borderColor: "#8e5ea2",
          fill: false
        }, { 
          data: neu_y,
          label: "NEU",
          borderColor: "#3cba9f",
          fill: false
        }
      ]
    },
    options: {
      title: {
        display: true,
        text: 'Sentiments change over sentences | Orginal Text'
      }, 
      scales: {
          xAxes: [{
              display: true,
               ticks: {
                 display: false
               },
              scaleLabel: {
                display: true,
                labelString: '# Sentence Index (x-axis)'
              }
          }],
          yAxes: [{
              display: true, 
              scaleLabel: {
                display: true,
                labelString: 'Sentiment Score (y-axis)'
              }
          }]
      }
    }
  });



  // =================== LINE CHART =======================
  // ===================== CHART =========================
  // =================== KEY_PHRASE  =======================

  var key_pharse_arr = news_index.visual_data.key_phrase_freq_algo
  max_count = 0; 
  algo_arr = Object.keys(key_pharse_arr); 

  $.each(key_pharse_arr, function(key,val){
    if(max_count < Object.keys(val).length)
      max_count = Object.keys(val).length; 
  }); 

  x_axis_key_phrase = []; 

  for(let i = 0; i < max_count; i++){
    x_axis_key_phrase.push(i); 
  }

  new Chart(document.getElementById("key_phrase_freq"), {
    type: 'line',
    data: {
      labels: x_axis_key_phrase,
      datasets: [{ 
          data: Object.values(key_pharse_arr[algo_arr[0]]),
          label: algo_arr[0], // texrank
          borderColor: "#3e95cd",
          fill: false
        }, { 
          data: Object.values(key_pharse_arr[algo_arr[1]]),
          label: algo_arr[1], // lexrank
          borderColor: "#8e5ea2",
          fill: false
        }, { 
          data: Object.values(key_pharse_arr[algo_arr[2]]),
          label: algo_arr[2], // lsa
          borderColor: "#3cba9f",
          fill: false
        }, { 
          data: Object.values(key_pharse_arr[algo_arr[3]]),
          label: algo_arr[3], // featuredAlgo
          borderColor: "#e8c3b9",
          fill: false
        }
      ]
    },
    options: {
      title: {
        display: true,
        text: 'Headline Key Terms Frequency Chart'
      }, 
       scales: {
          xAxes: [{
              display: true, 
              scaleLabel: {
                display: true,
                labelString: '# Sentence Index (x-axis)'
              }
          }],
          yAxes: [{
              display: true, 
              scaleLabel: {
                display: true,
                labelString: 'Term Frequency - tf (y-axis)'
              }
          }]
      }
    }
  });

  var spacy_ner_wc = news_index.visual_data.spacy_ner_wc; 
  ner_keys = Object.keys(spacy_ner_wc);
  ner_keys.push(" "); 

  ner_wc = Object.values(spacy_ner_wc);
  ner_wc.push(0); 
  
  color_arr = palette('tol', ner_keys.length).map(function(hex) { return '#' + hex; }); 

  // ============ Horizontal Bar CHART =======================
  // ===================== CHART =========================
  // =================== SPACY NERA =======================
  new Chart(document.getElementById("spacy_ner"), {
      type: 'horizontalBar',
      data: {
        labels: ner_keys,
        datasets: [
          {
            label: "NER Count",
            backgroundColor: color_arr,
            data: ner_wc
          }
        ]
      },
      options: {
        legend: { display: false },
        title: {
          display: true,
          text: 'Name Entity Recognition | Orginal Text'
        },

        scales: {
          xAxes: [{
              display: true, 
              scaleLabel: {
                display: true,
                labelString: '# NER Term Frequency (x-axis)'
              }
          }],
          yAxes: [{
              display: true, 
              scaleLabel: {
                display: true,
                labelString: ' NER Terms (y-axis)'
              }
          }]
      }
      }
    });
    

}

news_index.fnModalBuildGraph = function(i){
    // ChartJs  
  $('#chart_sect_1').append('<canvas id="headline_sentiment"></canvas>'); 
  $('#chart_sect_2').append('<canvas id="content_sentiment"></canvas>'); 

  let head = news_index.sentiment_data.headline; 
  let cont = news_index.sentiment_data.content;

  var ctx = document.getElementById('headline_sentiment').getContext('2d');
  var chart = new Chart(ctx, {
      // The type of chart we want to create
      type: 'bar',
      responsive: true, 
      maintainAspectRatio: false,

      // The data for our dataset
      data: {
          labels: ["POS", "NEG", "NEU", "COMPOUND"],
          datasets: [{
              label: "#sentiment",
              backgroundColor:[
                  'rgba(117, 255, 51, 0.2)',
                  'rgba(255, 54, 0, 0.2)',
                  'rgba(255, 206, 86, 0.2)',
                  'rgba(75, 192, 192, 0.2)'
              ],
              borderColor:[
                  'rgba(117, 255, 51, 0.2)',
                  'rgba(255, 54, 0, 0.2)',
                  'rgba(255, 206, 86, 0.2)',
                  'rgba(75, 192, 192, 0.2)'
              ] ,
              data: [head.pos, head.neg, head.neu, head.compound],
          }]
      },

      // Configuration options go here
      options: {
      title: {
          display: true,
          text: 'Headline Sentiments Bar Chart | Orginal Text'
        },
          scales: {
              xAxes: [{
                  stacked: true, 
                  display: true, 
                  scaleLabel: {
                    display: true,
                    labelString: 'Sentiment (x-axis)'
                  }

              }],
              yAxes: [{
                  stacked: true, 
                  display: true, 
                  scaleLabel: {
                    display: true,
                    labelString: 'Score (y-axis)'
                  }
              }]
          }
      }
  });


  var ctx = document.getElementById('content_sentiment').getContext('2d');
  var chart = new Chart(ctx, {
      // The type of chart we want to create
      type: 'bar',
      responsive: true, 
      maintainAspectRatio: false,
      // The data for our dataset
      data: {
          labels: ["POS", "NEG", "NEU", "COMPOUND" ],
          datasets: [{
              label: "#sentiment",
              backgroundColor:[
                  'rgba(117, 255, 51, 0.2)',
                  'rgba(255, 54, 0, 0.2)',
                  'rgba(255, 206, 86, 0.2)',
                  'rgba(75, 192, 192, 0.2)'
              ],
              borderColor:[
                  'rgba(117, 255, 51, 0.2)',
                  'rgba(255, 54, 0, 0.2)',
                  'rgba(255, 206, 86, 0.2)',
                  'rgba(75, 192, 192, 0.2)'
              ] ,
              data: [cont.pos, cont.neg, cont.neu, cont.compound],
          }]
      },

      // Configuration options go here
      options: {
        title: {
          display: true,
          text: 'Content Sentiments Bar Chart | Orginal Text'
        },
          scales: {
              xAxes: [{
                  stacked: true, 
                  display: true, 
                  scaleLabel: {
                    display: true,
                    labelString: 'Sentiment (x-axis)'
                  }
              }],
              yAxes: [{
                  stacked: true, 
                  display: true, 
                  scaleLabel: {
                    display: true,
                    labelString: 'Score (y-axis)'
                  }
              }]
          }
      }
  });
}

// Es6 Function 

news_index.fnFilterNews = (category) => {
  news_index.articles_filter = news_index.articles.filter((article)=>{return article.news_category === category});
  news_index.fnDisplayArticles(news_index.articles_filter); 
}

// NER TEXT HIGHLIGHT -
highlight = function(text, er_name='unknown') {
  var inputText = document.getElementById("summary_ner_text");
  var innerHTML = inputText.innerHTML;
  var regex = new RegExp('[,.]*( '+text+')[,. ]*', 'ig');;

  innerHTML = innerHTML.replace(regex, `&nbsp;<span class="tooltip">
                                          <span class="tooltiptext">${er_name}</span>
                                          <span class="highlight-${er_name.toLowerCase()}"> ${" "+ text + " "} </span>
                                        </span>&nbsp;`);

  inputText.innerHTML = innerHTML;
  
}

news_index.ner_highlighter = function(ner_dict){
  for(let key in ner_dict){
    highlight(key, ner_dict[key]); 
  }
}


var cur_tab = "all"; 
$(document).ready(function(){
  news_index.fnLoadArticles(); 

  $("[name='sentence_length_article']").change(function(){
    news_index.cur_sent_length = parseInt($("[name='sentence_length_article']").val()); 
    var summary_len = news_index.cur_sent_length; 
    news_index.fnSummaryAjax(news_index.cur_index, news_index.articles_filter, summary_len);
    news_index.ner_highlighter(news_index.visual_data.spacy_ner);
    news_index.fnSentimentAjax(news_index.cur_index, news_index.articles_filter); 
    news_index.fnVisualAjax(news_index.cur_index, news_index.articles_filter, summary_len); 

  }); 

  $("[name='optradio']").change(function(){
    news_index.fnSummaryAjax(news_index.cur_index, news_index.articles_filter); 
    news_index.ner_highlighter(news_index.visual_data.spacy_ner);
    $('.sum_algo').text( $('[name=optradio]:checked').val())

  }); 

  $("[tabs='category']").click(function(){
    if(this.value == 'all'){
      news_index.articles_filter = news_index.articles; 
      news_index.fnDisplayArticles(news_index.articles_filter);
    }
    else{ 
      news_index.fnFilterNews(this.value);
    }

    $(`[tab_name='${cur_tab}`).removeClass('w3-black');
    $(`[tab_name='${this.value}`).addClass('w3-black'); 
    cur_tab = this.value; 
  }); 



  $('#searchbar').keyup(function(event){
    if(cur_tab !== 'all'){
        $(`[tab_name='${cur_tab}`).removeClass('w3-black');
        cur_tab = "all";
        $(`[tab_name='${cur_tab}`).addClass('w3-black');

    }

    if(this.value.length > 3){

      var stringFindFunc = (article) => {
        let myPattern = new RegExp('(\\w*'+ this.value +'\\w*)','gi');
        let matches = article.article_text.match(myPattern);
        if(matches !== null)
          return true; 
      }

      news_index.articles_filter = news_index.articles.filter(stringFindFunc);

      news_index.fnDisplayArticles(news_index.articles_filter); 
    }
    else if(this.value == ''){
      news_index.fnDisplayArticles(news_index.articles);
    }
  }); 

}); 


