import json
import pymongo 

client = pymongo.MongoClient("mongodb://localhost:27017/")

db = client['bitnews']

col = db['abstract_articles']


with open("sample_data/sum.json") as json_file:
    json_data = json.load(json_file)
    
    ex_arr = json_data['AbsSum']['example']

    for elem in ex_arr:
    	x = col.insert_one({
    		'text' : elem['article'], 
			'ref_title' : elem['reference'], 
			'title'		: elem['summary'], 
			'BLEU'		: elem['eval']['BLEU']['_score'], 
			'ROUGE_1'	: elem['eval']['ROUGE_1']['_score'], 
			'ROUGE_2'   : elem['eval']['ROUGE_2']['_score'], 
			'ROUGE_l'	: elem['eval']['ROUGE_l']['_score'], 
			'ROUGE_be'  : elem['eval']['ROUGE_be']['_score']
		})
    	# print(elem['article']) # text 
    	# print(elem['reference']) # ref_title 
    	# print(elem['summary']) # title 
    	# print(elem['eval']['BLEU']['_score'])
    	# print(elem['eval']['ROUGE_1']['_score'])
    	# print(elem['eval']['ROUGE_2']['_score'])
    	# print(elem['eval']['ROUGE_l']['_score'])
    	# print(elem['eval']['ROUGE_be']['_score'])




