import json
from flask import Flask, render_template, flash, redirect, session, logging, request, make_response
from flask_pymongo import PyMongo
from forms import RegisterForm
from urllib.parse import unquote
from passlib.hash import sha256_crypt 
from bson.objectid import ObjectId
from functools import wraps 

### main class for summarization 
from summarizer import Summarizer

from nltk.sentiment.vader import SentimentIntensityAnalyzer as SIA

## CUSTOM ANALYTICS MODULE 
from analytics import summary_wc_algo
from analytics import senti_points_text_head
from analytics import senti_points_sentence_wise
from analytics import key_phrase_freq_algo
from analytics import spacy_ner
from analytics import spacy_ner_wc

app = Flask(__name__)
app.config["MONGO_URI"] = "mongodb://localhost:27017/bitnews"

mongo = PyMongo(app)


#------------------------------------------------------
# ----------------  DECORATOR FUNCTIONS ---------------
# -----------------------------------------------------


def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
    	if(session.get('logged_in') == None or session.get('logged_in') == False):            
    		return render_template('home.html')
    	return f(*args, **kwargs)
    return decorated_function



#------------------------------------------------------
# -------------------PAGE ROUTES ----------------------
# -----------------------------------------------------

@app.route('/')
def home():
	return render_template('home.html')

@app.route('/index')
@login_required
def index():
	app.logger.info(session.get('logged_in'))
	return render_template('index.html', user_name = session.get('user_name'))

@app.route('/crawler')
def crawler():
	return render_template('crawler.html', user_name = session.get('user_name'))

@app.route('/addarticle')
def addarticle():
	return render_template('addarticle.html', user_name = session.get('user_name'))

@app.route('/articles')
def articles():
	article_list = mongo.db.articles.find({})
	app.logger.info(article_list)
	return render_template("articles.html", articles=article_list)


@app.route('/article/<string:id>')
def article(id):
	return render_template('article.html', id=id)

@app.route('/summarizer_page')
def summarizer_page():
	return render_template("summarizer.html")

@app.route('/timeline')
def timline():
	return render_template("timeline.html")

#------------------------------------------------------
# -------------------- REST APIS ----------------------
# -----------------------------------------------------

@app.route('/logout')
def logout():
	session['logged_in'] = False
	session['username']	= ''
	return render_template('home.html')

@app.route('/register', methods=['GET', 'POST'])
def register():
	app.logger.info(request.json);

	user_name = request.json['user_name']
	user_email = request.json['user_email']
	user_dob   = request.json['user_dob']
	user_gender = request.json['user_gender']
	user_news_cat = request.json['user_news_cat']
	user_password = sha256_crypt.encrypt(str(request.json['user_password']))

	data = ''
	
	coll_email_count = mongo.db.users.find({"user_email" : user_email}).count()
	coll_user_count =  mongo.db.users.find({"user_name" : user_name}).count()
	app.logger.info(coll_email_count)
	app.logger.info(coll_user_count)

	if coll_email_count != 0 or coll_user_count != 0: 
		data = 'already_exists'

		if coll_email_count != 0:
			data = 'email_already_exists'
		elif coll_user_count != 0:
			data = 'username_already_exists'
	else: 
		try:
			mongo.db.users.insert({
				"user_name" : user_name, 
				"user_email" : user_email, 
				"user_dob"	: user_dob, 
				"user_gender" : user_gender, 
				"user_news_cat" : user_news_cat, 
				"user_password" : user_password
			})

			data='registered' 
			session['logged_in'] = True
			session['user_name'] = username
		except: 
			app.logger.info('error inserting into the database')

	return json.dumps({'success':True, 'data': data}), 200, {'ContentType':'application/json'}


@app.route('/login', methods=['GET', 'POST'])
def login():
	if request.method == 'POST': 
		# Get Form Fields 

		username = request.json['username']
		password_candidate = request.json['password']

		data = ''

		try: 
			result = mongo.db.users.find({"user_name" : username})
			
			if result.count() == 0:
				data = 'no_user'
			else: 
				app.logger.info(result[0]['user_name'])

				#compare passwords 
				if sha256_crypt.verify(password_candidate, result[0]['user_password']): 
					app.logger.info('password_match')
					data = 'login_success'
					session['logged_in'] = True
					session['user_name'] = username
				else: 
					data = 'password_invalid'
					app.logger.info('password_invalid')
		except: 
			app.logger.info("database error")

	return  json.dumps({'success':True, 'data': data}), 200, {'ContentType':'application/json'}


@app.route('/summarizer', methods=['GET', 'POST'])
def summarizer():
	if request.method == 'POST':  
		headline = request.json['headline']
		content = request.json['content']
		algo 	= request.json['algorithm']
		sentence_length = request.json['sentence_length']
		summary = ""
		if algo == "lsa":
			summary = Summarizer.lsa_summarizer(content, sentence_length)
		elif algo == "luhn": 
			summary = Summarizer.luhn_summarizer(content, sentence_length)

		elif algo == "textrank":
			summary = Summarizer.textrank_summarizer(content, sentence_length)

		elif algo == "lexrank": 
			summary = Summarizer.lexrank_summarizer(content, sentence_length)

		elif algo == "sumbasic": 
			summary = Summarizer.lsa_summarizer(content, sentence_length)

		elif algo == "featured": 
			summary = Summarizer.featured_summarizer(content, headline, sentence_length)

		return json.dumps({'success':True, 'data': summary}), 200, {'ContentType':'application/json'}

@app.route('/load_articles',  methods=['GET', 'POST'])
def load_articles():
	article_list = mongo.db.crawl_articles.find({}).sort([('date_publish', 1)])  

	articles_arr = []
	for article in article_list: 
		
		article_new = {
		"url" 			: "",
		"news_category" : "", 
		"source"		: "", 
		"date_publish"	: "",
		"description"	: "", 
		"image_url"		: "", 
		"article_text"  : "", 
		"article_title" : "", 
		"article_url"   : ""
		}

		#data mapping for frontend 
		article_new['url'] 					= article['url']
		article_new['news_category']	 	= str(article['news_category'])
		article_new['date_publish']			= str(article['date_publish'])
		article_new['description']			= article['description']
		article_new['image_url']			= article['image_url']
		article_new['article_url'] 			= article['article_url']
		article_new['article_text']			= article['article_text']
		article_new['article_title']		= article['article_title']
		article_new['source']				= article['source']
		article_new['summary']				= Summarizer.textrank_summarizer(article['article_text'])

		articles_arr.append(article_new)

	return json.dumps({'success':True, 'data': articles_arr}), 200, {'ContentType':'application/json'}

@app.route('/sentiments', methods=['GET', 'POST'])
def sentiments(): 
	sia = SIA()
	if request.method == 'POST':  
		headline = request.json['headline']
		content = request.json['content']

		sentiment_arr = {
			"headline": "",
			"content" : ""
		}
		
		sentiment_scores_headline = sia.polarity_scores(headline)
		sentiment_scores_content = sia.polarity_scores(content)

		sentiment_arr['headline'] = sentiment_scores_headline 
		sentiment_arr['content']  = sentiment_scores_content
		
		return json.dumps({'success':True, 'data':  sentiment_arr }), 200, {'ContentType':'application/json'}


@app.route('/visualize', methods=['GET', 'POST'])
def visualize (): 
	if request.method  == 'POST': 
		headline = request.json['headline']
		content = request.json['content']
		sentence_length = request.json['sentence_length']

		visual_arr = {
			"summary_wc_algo" : summary_wc_algo(content, headline, sentence_length), 
			"senti_points_text_head" : senti_points_text_head(content, headline), 
			"senti_points_sentence_wise" : senti_points_sentence_wise(content), 
			"key_phrase_freq_algo" : key_phrase_freq_algo(content, headline, sentence_length), 
			"spacy_ner"			   : spacy_ner(content), 
			"spacy_ner_wc"		   : spacy_ner_wc(content)
		}

		return json.dumps({'success':True, 'data':  visual_arr }), 200, {'ContentType':'application/json'}


@app.route('/add_article', methods=['GET', 'POST'])
def add_article():
	if request.method == 'POST':


		if(mongo.db.crawl_articles.find({'url': request.json['article_url']}).count() > 0):
			print('article already exists')
		else: 
			x = mongo.db.crawl_articles.insert_one({
			"url" : request.json['article_url'],
			"news_category" : request.json['news_category'], 
			"source"		: request.json['source'], 
			"date_publish" 	: request.json['date_publish'],
			"description"	: request.json['description'],
			"image_url"		: request.json['image_url'],
			"article_text"	: request.json['article_text'],
			"article_title" : request.json['article_title'],
			"article_url" 	: request.json['article_url'] 
			})

			print("Article Inserted  " + request.json['source'] , request.json['article_url'])
			

		return json.dumps({'success':True, 'data':  'success' }), 200, {'ContentType':'application/json'}


@app.route('/abstract_articles', methods=['GET','POST'])
def abstract_articles():
	article_list = mongo.db.abstract_articles.find({}) 

	articles_arr = []
	for article in article_list: 
		
		article_new = {
		"text"      : "", 
		"title"		: "", 
		"ref_title" : "", 
		"BLEU"		: "", 
		"ROUGE_1"   : "", 
		"ROUGE_2"   : "", 
		"ROUGE_l"   : "", 
		"ROUGE_be"  : ""
 		}

		#data mapping for frontend 
		article_new['text']  	= article['text']
		article_new['title']	= article['title']
		article_new['ref_title'] = article['ref_title']
		article_new['BLEU']		 = article['BLEU']
		article_new['ROUGE_1']   = article['ROUGE_1']
		article_new['ROUGE_2']  = article['ROUGE_2']
		article_new['ROUGE_l']  = article['ROUGE_l']
		article_new['ROUGE_be'] = article['ROUGE_be']

		articles_arr.append(article_new)


	return json.dumps({'success':True, 'data': articles_arr}), 200, {'ContentType':'application/json'}



if __name__=='__main__':
	app.secret_key = 'jmf57nt4#T'
	app.run(debug=True)




